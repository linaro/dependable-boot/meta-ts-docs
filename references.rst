##########
References
##########

.. [UEFI] `Unified Extensable Firmware Interface Specification v2.9
   <https://uefi.org/sites/default/files/resources/UEFI_Spec_2_9_2021_03_18.pdf>`_,
   February 2020, `UEFI Forum <http://www.uefi.org>`_

.. [EBBR] `Embedded Base Boot Requirements v2.0.0-pre1
   <https://arm-software.github.io/ebbr/>`_,
   January 2021, `Arm Limited <http://arm.com>`_

.. [fTPM] `Firmware TPM
   <https://www.microsoft.com/en-us/research/publication/ftpm-software-implementation-tpm-chip/>`_,
   August 2016, `Microsoft <http://www.microsoft.com>`_
